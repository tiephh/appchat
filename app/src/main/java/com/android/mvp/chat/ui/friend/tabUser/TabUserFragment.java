package com.android.mvp.chat.ui.friend.tabUser;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.friend.HomeFriendFragment;
import com.android.mvp.chat.utils.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabUserFragment extends BaseFragment implements TabUserMvpView {

    @Inject
    TabUserMvpPresenter<TabUserMvpView> mPresenter;

    @BindView(R.id.recycler_view_tab_user)
    RecyclerView rvUsers;

    private TabUserAdapter adapter;

    public static TabUserFragment newInstance(Bundle args) {
        TabUserFragment fragment = new TabUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_all_user, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        HomeFriendFragment homeFriendFragment = (HomeFriendFragment) getParentFragment();
        homeFriendFragment.setOnTextChangedUser(new HomeFriendFragment.OnTextChangedUser() {
            @Override
            public void onTextChangedUser(String word) {
                mPresenter.searchUser(word);
            }
        });
    }

    private void initAdapter() {
        rvUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TabUserAdapter(getContext(), new ArrayList<>());
        rvUsers.setAdapter(adapter);
    }

    @Override
    public void onSuccess(List<UserSort> userArrayList) {
        if (adapter != null) {
            adapter.setData(userArrayList);

            adapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void sendRequestFr(int position) {
                    mPresenter.sendRequestFr(userArrayList.get(position).getUsers());
                }

                @Override
                public void cancleRequestFr(int position) {
                    mPresenter.cancleRequestFr(userArrayList.get(position).getUsers());
                }
            });
        }
    }

}
