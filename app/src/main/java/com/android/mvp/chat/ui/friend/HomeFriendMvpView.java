package com.android.mvp.chat.ui.friend;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface HomeFriendMvpView extends MvpView {
    void readRequest(ArrayList<Users> userArrayList);
}
