package com.android.mvp.chat.ui.friend.tabUser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.listener.OnItemClickListener;
import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TabUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<UserSort> userList;

    public static final int SECTION_VIEW = 1;
    public static final int CONTENT_VIEW = 2;

    public TabUserAdapter(Context context, List<UserSort> userList) {
        this.context = context;
        this.userList = userList;
    }

    public void setData(List<UserSort> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }

    public OnItemClickListener onItemClickListener;

    void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;

        switch (viewType) {
            case SECTION_VIEW:
                view = LayoutInflater.from(context).inflate(R.layout.item_user_header, viewGroup, false);
                return new MyViewHolderHead(view);
            default:
                view = LayoutInflater.from(context).inflate(R.layout.item_tab_user, viewGroup, false);
                return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        UserSort user = userList.get(i);
        switch (viewHolder.getItemViewType()) {
            case SECTION_VIEW:
                MyViewHolderHead myViewHolderHead = (MyViewHolderHead) viewHolder;
                myViewHolderHead.bindView(user);
                break;
            case CONTENT_VIEW:
                MyViewHolder myViewHolder = (MyViewHolder) viewHolder;
                myViewHolder.bindView(user, onItemClickListener, i);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (userList.get(position).isSection()) {
            return SECTION_VIEW;
        } else {
            return CONTENT_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.col_user_friend)
        TextView txtUsername;

        @BindView(R.id.ivc_avatar)
        CircleImageView imgCircleFriend;

        @BindView(R.id.layout_tab_fr)
        LinearLayout layoutTabFr;

        @BindView(R.id.btn_cancle_request_fr)
        AppCompatButton btnCancleRequestFr;

        @BindView(R.id.btn_send_request_fr)
        AppCompatButton btnSendRequestFr;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bindView(UserSort user, OnItemClickListener onItemClickListener, int i) {

            if (user.getStateUser() == AppConstants.StateUser.FRIEND) {
                btnCancleRequestFr.setVisibility(View.GONE);
                btnSendRequestFr.setVisibility(View.GONE);
            } else if (user.getStateUser() == AppConstants.StateUser.REQUEST_FRIEND) {
                btnCancleRequestFr.setVisibility(View.VISIBLE);
                btnSendRequestFr.setVisibility(View.GONE);
            } else {
                btnCancleRequestFr.setVisibility(View.GONE);
                btnSendRequestFr.setVisibility(View.VISIBLE);
            }

            txtUsername.setText(user.getUsers().getUsername());
            if (user.getUsers().getImageURL().equals(AppConstants.DEFAULT)) {
                Glide.with(context).load(R.mipmap.ic_launcher);
            } else {
                Glide.with(context).load(user.getUsers().getImageURL()).circleCrop().into(imgCircleFriend);
            }
            btnSendRequestFr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnCancleRequestFr.setVisibility(View.VISIBLE);
                    btnSendRequestFr.setVisibility(View.GONE);
                    if (onItemClickListener != null) {
                        onItemClickListener.sendRequestFr(getAdapterPosition());
                    }
                }
            });
            btnCancleRequestFr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnCancleRequestFr.setVisibility(View.GONE);
                    btnSendRequestFr.setVisibility(View.VISIBLE);
                    if (onItemClickListener != null) {
                        onItemClickListener.cancleRequestFr(getAdapterPosition());
                    }
                }
            });

        }
    }

    public class MyViewHolderHead extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_header)
        TextView txtHeader;


        public MyViewHolderHead(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bindView(UserSort user) {
            txtHeader.setText(user.getHeader());
        }
    }
}
