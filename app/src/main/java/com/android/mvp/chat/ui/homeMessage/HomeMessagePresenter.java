package com.android.mvp.chat.ui.homeMessage;

import androidx.annotation.NonNull;

import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class HomeMessagePresenter<V extends HomeMessageMvpView> extends BasePresenter<V>
        implements HomeMessageMvpPresenter<V> {

    private static final String TAG = "HomeMesagePresenter";


    @Inject
    public HomeMessagePresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getDataManager().getListRoomByUser().keepSynced(true);
        getDataManager().getChatRoomMessage().keepSynced(true);
        getDataManager().getDatabaseReference(AppConstants.ReferencePath.ROOMS)
                .child(getDataManager().getFirebaseUserId()).keepSynced(true);
        getListChatRoom();
    }

    private void getListChatRoom() {
        getDataManager().getListRoomByUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        ArrayList<Room> rooms = new ArrayList<>();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Room room = dataSnapshot.getValue(Room.class);
                            if (room != null && !room.getLastMessage().equals("")) {
                                rooms.add(room);
                            }
                        }
                        getMvpView().addDataRooms(rooms);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        getMvpView().showMessage(error.getMessage());
                    }
                });
    }

    @Override
    public void searchRoom(String word) {
        ArrayList<Room> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.ReferencePath.ROOMS)
                .child(getDataManager().getFirebaseUserId())
                .orderByChild(AppConstants.NAME)
                .startAt(word).endAt(word + "\uf8ff")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        userArrayList.clear();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Room user = dataSnapshot.getValue(Room.class);
                            assert user != null;
                            if (!user.getId().equals(getDataManager().getFirebaseUserId()) && !user.getLastMessage().equals("")) {
                                userArrayList.add(user);
                            }
                        }
                        getMvpView().addDataRooms(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
}
