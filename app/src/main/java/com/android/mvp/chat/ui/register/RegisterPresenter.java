package com.android.mvp.chat.ui.register;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class RegisterPresenter <V extends RegisterMvpView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V> {


    private static final String TAG = "RegisterPresenter";
    @Inject
    public RegisterPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onRegister(String userName, String email, String pass, boolean isCheck) {
        if(TextUtils.isEmpty(userName) || TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)){
            getMvpView().failInputData();
        }else if(pass.length() < 6){
            getMvpView().failPassWord();
        }else {
            if(isCheck){
                register(userName, email, pass);
            }else {
                getMvpView().failPolicy();
            }
        }
    }

    @Override
    public void onLogin(String user, String pass) {
        getDataManager().getSignInFirebase(user, pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            getMvpView().openHomeFragment();
                        }else {
                            getMvpView().onFail();
                        }
                    }
                });;
    }

    private  void register(String userName, String email, String password){
        getDataManager().getCreateFirebase(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser firebaseUser = getDataManager().getFirebaseAuth().getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap. put(AppConstants.ID_RES, userid);
                            hashMap.put(AppConstants.USER_NAME_RES, userName);
                            hashMap.put(AppConstants.IMG_RES, AppConstants.DEFAULT);
                            getDataManager().getDatabaseReference(AppConstants.USERS).child(userid).setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        getMvpView().onSuccess();
                                    }
                                }
                            });
                        }else{
                            getMvpView().onFail();
                        }
                    }
                });
    }
}
