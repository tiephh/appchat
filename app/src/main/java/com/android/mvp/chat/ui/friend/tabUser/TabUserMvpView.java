package com.android.mvp.chat.ui.friend.tabUser;

import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpView;

import java.util.ArrayList;
import java.util.List;

public interface TabUserMvpView extends MvpView {
    void onSuccess(List<UserSort> userArrayList);

}
