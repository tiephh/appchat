package com.android.mvp.chat.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.EventBusMes;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.friend.HomeFriendFragment;
import com.android.mvp.chat.ui.homeMessage.HomeMessageFragment;
import com.android.mvp.chat.ui.profile.HomeProfileFragment;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements HomeMvpView {

    public static final String TAG = HomeFragment.class.getSimpleName();

    @Inject
    HomeMvpPresenter<HomeMvpView> mPresenter;


    @BindView(R.id.bottom_nav_home)
    BottomNavigationView bottomNavHome;

    @BindView(R.id.view_pager)
    ViewPager viewPager;


    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(EventBusMes event) {
        viewPager.setCurrentItem(event.getMessage());
    }

    @Override
    protected void setUp(View view) {
        setUpViewPager();
        bottomNavHome.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_messenger:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.action_friend:
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.action_profile:
                        viewPager.setCurrentItem(2);
                        break;
                }
                return true;
            }
        });
        mPresenter.readRequest();
        mPresenter.readRooms();
    }

    private void setUpViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(HomeMessageFragment.newInstance());
        viewPagerAdapter.addFragment(HomeFriendFragment.newInstance());
        viewPagerAdapter.addFragment(HomeProfileFragment.newInstance());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(viewPagerAdapter.getCount());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        bottomNavHome.getMenu().findItem(R.id.action_messenger).setChecked(true);
                        break;
                    case 1:
                        bottomNavHome.getMenu().findItem(R.id.action_friend).setChecked(true);
                        break;
                    case 2:
                        bottomNavHome.getMenu().findItem(R.id.action_profile).setChecked(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void readRooms(int count) {
        if(bottomNavHome != null){
            BadgeDrawable badgeDrawable = bottomNavHome.getOrCreateBadge(R.id.action_messenger);
            if (count > 0) {
                badgeDrawable.setVisible(true);
                badgeDrawable.setNumber(count);
            } else {
                badgeDrawable.setVisible(false);
            }
        }
    }

    @Override
    public void readRequest(ArrayList<Users> users) {
        if(bottomNavHome != null) {
            BadgeDrawable badgeDrawable2 = bottomNavHome.getOrCreateBadge(R.id.action_friend);
            if (users.size() > 0) {
                badgeDrawable2.setVisible(true);
                badgeDrawable2.setNumber(users.size());
            } else {
                badgeDrawable2.setVisible(false);
            }
        }
    }
}
