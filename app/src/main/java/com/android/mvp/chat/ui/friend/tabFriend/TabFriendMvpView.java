package com.android.mvp.chat.ui.friend.tabFriend;

import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface TabFriendMvpView extends MvpView {
    void onSuccess(ArrayList<SortUser> userArrayList);

    void openChatRoom(Room room);
}
