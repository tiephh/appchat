package com.android.mvp.chat.ui.friend.request;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.utils.AppConstants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SenderAdapter extends RecyclerView.Adapter<SenderAdapter.SenderViewHolder>{

    private Context mContext;
    private ArrayList<Users> data;

    public SenderAdapter(Context mContext, ArrayList<Users> data) {
        this.mContext = mContext;
        this.data = data;
    }
    public interface IOnClickItemSender{
        void itemClick(Users users);
    }
    public IOnClickItemSender iOnClickItem;

    public void setOnClickItemSender(IOnClickItemSender iOnClickItem) {
        this.iOnClickItem = iOnClickItem;
    }

    public void setData(ArrayList<Users> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SenderAdapter.SenderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_request_cancle_friend, viewGroup, false);
        return new SenderAdapter.SenderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SenderAdapter.SenderViewHolder receiverViewHolder, int i) {
        Users users = data.get(i);
        receiverViewHolder.bindView(users, iOnClickItem);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    class SenderViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ivc_avatar)
        CircleImageView ivcAvater;

        @BindView(R.id.col_user_friend)
        TextView txtName;

        @BindView(R.id.btn_cancle_friend)
        AppCompatButton btnCancleFriend;

        public SenderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        public void bindView(Users users, IOnClickItemSender iOnClickItem){
            txtName.setText(users.getUsername());
            if (users.getImageURL().equals(AppConstants.DEFAULT)) {
                Glide.with(mContext).load(R.drawable.grapefruit);
            } else {
                Glide.with(mContext).load(users.getImageURL()).circleCrop().into(ivcAvater);
            }
            btnCancleFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (iOnClickItem != null){
                        iOnClickItem.itemClick(users);
                    }
                }
            });
        }
    }
}
