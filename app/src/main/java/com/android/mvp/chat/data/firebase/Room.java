package com.android.mvp.chat.data.firebase;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Room implements Serializable {
    private String id;
    private String name;
    private String thumbnail;
    private String receiver;
    private int unreadMessage;
    private String lastMessage;
    private String timeStamp;

    public Room() {
    }

    public Room(String id, String name, String thumbnail, String receiver, int unreadMessage, String lastMessage, String timeStamp) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
        this.receiver = receiver;
        this.unreadMessage = unreadMessage;
        this.lastMessage = lastMessage;
        this.timeStamp = timeStamp;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("name", name);
        result.put("thumbnail", thumbnail);
        result.put("unreadMessage", unreadMessage);
        result.put("lastMessage", lastMessage);
        result.put("receiver", receiver);
        result.put("timeStamp", timeStamp);
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getUnreadMessage() {
        return unreadMessage;
    }

    public void setUnreadMessage(int unreadMessage) {
        this.unreadMessage = unreadMessage;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
