package com.android.mvp.chat.ui.homeMessage.messageUser;

import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.ui.base.MvpPresenter;

import java.util.ArrayList;

public interface MessageUserMvpPresenter<V extends MessageUserMvpView> extends MvpPresenter<V> {

    void setRoom(Room room);

    void sendMessage(String message);

    void sendImg(ArrayList<String> listImg);
    void sendStick(String name);
}
