package com.android.mvp.chat.ui.homeMessage;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.DateUtils;
import com.android.mvp.chat.utils.listener.OnItemClickListener;
import com.bumptech.glide.Glide;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Room> data;

    public interface OnItemClick{
        void onItemClick(int position);
    }
    public OnItemClick listener;

    void setOnItemClickListener(OnItemClick listener) {
        this.listener = listener;
    }

    public RoomAdapter(Context context, ArrayList<Room> data) {
        this.context = context;
        this.data = data;
    }

    public void setData(ArrayList<Room> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ArrayList<Room> getData() {
        return data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Room room = this.data.get(position);
        try {
            holder.bind(room, listener);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_name)
        TextView username;
        @BindView(R.id.img_circle_home_mess)
        ImageView profilePicture;
        @BindView(R.id.txt_unread_message)
        TextView tvUnread;
        @BindView(R.id.item_home_messenger)
        ConstraintLayout rlContent;
        @BindView(R.id.txt_last_message)
        TextView tvLastMes;

        @BindView(R.id.tv_time)
        TextView tvTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private String convertTime(long time) {
            Date date = new Date(time);
            Format format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            return format.format(date);
        }

        private String getCurrentTime() {
            Long tsLong = System.currentTimeMillis();
            String ts = tsLong.toString();
            return ts;
        }

        private String checktime(Room room) throws ParseException {
            String time = convertTime(Long.parseLong(room.getTimeStamp()));
            Date dateTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(time);

            DateUtils dateUtils = new DateUtils(Calendar.getInstance().getTime());
            DateUtils dateMess = new DateUtils(dateTime);

            String month = dateUtils.getMonth();
            String month2 = dateMess.getMonth();

            String date = dateUtils.getDate();
            String date2 = dateMess.getDate();

            if (month.equals(month2)) {
                if (Integer.parseInt(date) - Integer.parseInt(date2) == 1) {
                    return context.getString(R.string.yesterday);
                } else if (Integer.parseInt(date) - Integer.parseInt(date2) > 1) {
                    return dateMess.getYear();
                } else {
                    return dateMess.getHour();
                }
            } else {
                return dateMess.getYear();
            }
        }

        public void bind(Room room, OnItemClick listener) throws ParseException {
            username.setText(room.getName());
            Typeface typeBoldName = ResourcesCompat.getFont(context, R.font.lato_bold);
            username.setTypeface(typeBoldName);
            if (room.getUnreadMessage() > 0 && room.getUnreadMessage() < 9) {
                tvUnread.setVisibility(View.VISIBLE);
                Typeface typeBold = ResourcesCompat.getFont(context, R.font.lato_bold);
                tvLastMes.setTextColor(context.getColor(R.color.black));
                tvLastMes.setTypeface(typeBold);
                tvUnread.setText(String.valueOf(room.getUnreadMessage()));
            }else if(room.getUnreadMessage() > 9){
                tvUnread.setText("9+");
            }else {
                tvUnread.setVisibility(View.GONE);
                Typeface typeNormal = ResourcesCompat.getFont(context, R.font.lato_normal);
                tvLastMes.setTypeface(typeNormal);
                tvLastMes.setTextColor(context.getColor(R.color.gray));
            }
            tvLastMes.setText(room.getLastMessage());

            tvTime.setText(checktime(room));
            if (room.getThumbnail() != null && !room.getThumbnail().equals(AppConstants.DEFAULT)) {
                Glide.with(context).load(room.getThumbnail()).into(profilePicture);
            } else {
                Glide.with(context).load(R.mipmap.ic_launcher).circleCrop().into(profilePicture);
            }
            rlContent.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}